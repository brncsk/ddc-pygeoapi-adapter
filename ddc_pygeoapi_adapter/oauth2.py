from os import environ
from functools import wraps

from flask import Flask, request, session, g
from authlib.integrations.flask_oauth2 import (
    AuthorizationServer,
    ResourceProtector,
)
from authlib.integrations.sqla_oauth2 import (
    create_query_client_func,
    create_save_token_func,
    create_revocation_endpoint,
    create_bearer_token_validator,
)
from authlib.oauth2.rfc6749 import HttpRequest, grants

from .db import db
from .models import Client, Token


resource_protector = ResourceProtector()
query_client = create_query_client_func(db.session, Client)
save_token = create_save_token_func(db.session, Token)

authorization = AuthorizationServer(
    query_client=query_client,
    save_token=save_token,
)


class ClientCredentialsGrant(grants.ClientCredentialsGrant):
    TOKEN_ENDPOINT_AUTH_METHODS = ["client_secret_basic", "client_secret_post"]
    TOKEN_ENDPOINT_HTTP_METHODS = ["GET", "POST"]


def config_oauth(app: Flask):
    authorization.init_app(app)

    # We only support Client Credentials Flow
    authorization.register_grant(ClientCredentialsGrant)

    # Allow revocation of tokens
    revocation_cls = create_revocation_endpoint(db.session, Token)
    authorization.register_endpoint(revocation_cls)

    # Register token validator
    bearer_cls = create_bearer_token_validator(db.session, Token)
    resource_protector.register_token_validator(bearer_cls())

    def register_token_in_global_context(token):
        g.token = token

    app.before_request(authenticate(register_token_in_global_context))


def authenticate(wrapped_fn):
    @wraps(wrapped_fn)
    def wrapper():
        # Skip authentication if debug mode is enabled
        if environ.get("FLASK_DEBUG") == "True":
            # ...but still set the token in the global context to a mock value
            g.token = Token(
                client_id=environ.get("OAUTH2_CLIENT_ID"),
                user_id=None,
                access_token="debug",
                token_type="Bearer",
                expires_in=3600,
            )
            return None

        # Skip authentication if request is to the oauth token endpoint
        if request.path == "/oauth/token":
            return None

        try:
            if "token" in session:
                # Use token from session if it exists
                token = resource_protector.validate_request(
                    None,
                    HttpRequest(
                        method=request.method,
                        uri=request.full_path,
                        data=None,
                        headers={
                            **request.headers,
                            "Authorization": f'Bearer {session["token"]}',
                        },
                    ),
                )
            else:
                # Otherwise, use token from request
                token = resource_protector.acquire_token()
        except Exception as e:
            return {"code": "Unauthorized", "description": str(e)}, 401

        return wrapped_fn(token)

    return wrapper
