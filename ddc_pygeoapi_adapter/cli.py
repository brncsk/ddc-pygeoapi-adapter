from pygeoapi.flask_app import CONFIG

from .app import app


def main():
    """Main entry point for the application"""

    app.run(
        host=CONFIG["server"]["bind"]["host"],
        port=CONFIG["server"]["bind"]["port"],
        debug=True,
        use_reloader=False,
    )
