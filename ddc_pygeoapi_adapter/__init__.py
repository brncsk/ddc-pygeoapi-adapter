from dotenv import load_dotenv, find_dotenv

# Load the .env file if it exists.
load_dotenv(find_dotenv(filename=".env"))

# Load the .env.development file if it exists, overriding any existing values.
load_dotenv(find_dotenv(filename=".env.development"), override=True)
