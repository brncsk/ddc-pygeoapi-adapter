from os import environ
import time

from typing import List, Optional
from typing_extensions import Annotated

from sqlalchemy import String, ForeignKey
from sqlalchemy.orm import (
    DeclarativeBase,
    Mapped,
    MappedAsDataclass,
    mapped_column,
    relationship,
    registry,
)

import pyproj
from shapely.geometry import box
from shapely.ops import transform

ClientId = Annotated[str, 40]
ClientSecret = Annotated[str, 55]
TokenText = Annotated[str, 255]

mapper_registry = registry(
    type_annotation_map={
        ClientId: String(40),
        ClientSecret: String(55),
        TokenText: String(255),
    }
)

# The CRS in which bounding boxes are stored in the database
BBOX_DEFAULT_CRS_ID = environ.get("BBOX_DEFAULT_CRS", "EPSG:4326")
BBOX_DEFAULT_CRS = pyproj.CRS(BBOX_DEFAULT_CRS_ID)

# Compute the area of the bounding box in Web Mercator (EPSG:3857) by default
BBOX_AREA_CRS_ID = environ.get("BBOX_AREA_CRS", "EPSG:3857")
BBOX_AREA_CRS = pyproj.CRS(BBOX_AREA_CRS_ID)

project = pyproj.Transformer.from_crs(
    BBOX_DEFAULT_CRS, BBOX_AREA_CRS, always_xy=True
).transform


class Base(DeclarativeBase, MappedAsDataclass):
    registry = mapper_registry


class Client(Base):
    """OAuth2 client model"""

    __tablename__ = "oauth_client"

    client_id: Mapped[ClientId] = mapped_column(primary_key=True)
    client_secret: Mapped[ClientSecret] = mapped_column(unique=True, index=True)

    tokens: Mapped[Optional[List["Token"]]] = relationship(cascade="all, delete-orphan")

    @property
    def default_redirect_uri(self):
        return ""

    @property
    def default_scopes(self):
        return []

    @property
    def client_type(self):
        return "confidential"

    @property
    def allowed_grant_types(self):
        return ["client_credentials"]

    @property
    def allowed_auth_methods(self):
        return ["client_secret_basic", "client_secret_post"]

    def __repr__(self):
        return f"<Client {self.client_id}>"

    def check_client_secret(self, client_secret):
        return self.client_secret == client_secret

    def check_endpoint_auth_method(self, method, endpoint):
        return method in self.allowed_auth_methods and endpoint == "token"

    def check_grant_type(self, grant_type):
        return grant_type in self.allowed_grant_types


class Token(Base):
    """OAuth2 token model"""

    __tablename__ = "oauth_token"

    access_token: Mapped[TokenText] = mapped_column(primary_key=True)
    client_id: Mapped[ClientId] = mapped_column(ForeignKey("oauth_client.client_id"))
    user_id: Mapped[int] = mapped_column(nullable=True)

    expired_at: Mapped[int] = mapped_column(nullable=True)
    expires_in: Mapped[int]
    token_type: Mapped[str]

    def __init__(self, /, client_id, user_id, access_token, token_type, expires_in):
        self.client_id = client_id
        self.user_id = user_id
        self.access_token = access_token
        self.token_type = token_type
        self.expires_in = expires_in
        self.expired_at = time.time() + self.expires_in

        super().__init__()

    def is_expired(self):
        if not self.expired_at:
            return False

        return self.expired_at < time.time()

    def is_revoked(self):
        return False

    def get_scope(self):
        return ""

    def __repr__(self):
        return f"<Token {self.access_token}>"


class Order(Base):
    """Data request model"""

    __tablename__ = "order"

    id: Mapped[int] = mapped_column(primary_key=True)

    client_id: Mapped[ClientId] = mapped_column(ForeignKey("oauth_client.client_id"))

    collection_id: Mapped[str] = mapped_column()
    bbox: Mapped[str] = mapped_column(nullable=True)

    ts: Mapped[int] = mapped_column()
    fulfilled: Mapped[bool] = mapped_column(default=False)
    time_taken_ms: Mapped[int] = mapped_column(default=0)
    error: Mapped[str] = mapped_column(default="")

    def __init__(self, /, client_id, collection_id, bbox):
        self.client_id = client_id
        self.collection_id = collection_id
        self.ts = time.time()

        try:
            self.bbox = ",".join(map(str, map(float, bbox.split(","))))
        except Exception:
            self.bbox = None

        super().__init__()

    def add_error(self, error: str):
        if self.error == "" or self.error is None:
            self.error = error
        else:
            self.error += f"; {error}"

    @staticmethod
    def time_ms():
        return time.time_ns() // 1e6

    def settle(
        self,
        fulfilled: bool = True,
        error: str = None,
    ):
        self.time_taken_ms = self.time_ms() - self.ts * 1000
        self.fulfilled = fulfilled

        if error:
            self.add_error(error)

    @property
    def area(self):
        bbox = box(*map(float, self.bbox.split(",")))
        return transform(project, bbox).area
