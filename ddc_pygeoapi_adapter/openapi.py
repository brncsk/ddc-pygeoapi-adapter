from os import environ
from pathlib import Path
import yaml

from flask import current_app
from flasgger import Swagger

from pygeoapi.openapi import get_oas
from pygeoapi.log import LOGGER
from pygeoapi.flask_app import CONFIG

OPENAPI_FILE = Path(__file__).parent.parent / "assets" / "openapi.yml"

FLASGGER_SPEC = None


def generate_flasgger_spec():
    """Generates our Flasgger OpenAPI document.

    NOTE: This is memoized because it has to be run in the context of a Flask
          application, and dosoremi.map_list_cache_thread is run in a separate
          thread.
          We ensure that this is run from the main thread for the first time
          in app.init_app().
    """

    # pylint: disable=W0603
    global FLASGGER_SPEC

    if FLASGGER_SPEC is None:
        swagger = Swagger(current_app)
        FLASGGER_SPEC = swagger.get_apispecs()

    return FLASGGER_SPEC


def generate_openapi():
    """Generates our OpenAPI document by merging the pygeoapi document with
    our customizations via Flasgger.
    """
    LOGGER.info("Generating OpenAPI document")

    pygeoapi_spec = get_oas(CONFIG)
    flasgger_spec = generate_flasgger_spec()

    # Merge the two documents
    spec = pygeoapi_spec.copy()
    spec["paths"].update(flasgger_spec["paths"])

    # Generate OpenAPI YAML file
    with open(OPENAPI_FILE, "w", encoding="utf-8") as f:
        f.write(yaml.safe_dump(spec, default_flow_style=False))

    environ["PYGEOAPI_OPENAPI"] = str(OPENAPI_FILE)
