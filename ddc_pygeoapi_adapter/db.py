from os import environ

from flask import Flask
from sqlalchemy import Engine, create_engine
from sqlalchemy.orm import sessionmaker, Session

from .models import Base, Client


class Database:
    session: Session
    engine: Engine

    def __init__(self) -> None:
        self.engine = create_engine(environ.get("DATABASE_URL"))
        self.session = sessionmaker(bind=self.engine)()

        try:
            Base.metadata.create_all(self.engine)
            self.insert_default_client()
        except Exception:
            pass

    def init_app(self, app: Flask) -> None:
        app.teardown_appcontext(self.teardown)

    def insert_default_client(self) -> None:
        # Insert the default OAuth client (ddc:ddc), if the table is empty
        if self.session.query(Client).count() == 0:
            try:
                client = Client(
                    client_id=environ.get("OAUTH_CLIENT_ID"),
                    client_secret=environ.get("OAUTH_CLIENT_SECRET"),
                    tokens=[],
                )

                self.session.add(client)
                self.session.commit()
            except Exception:
                self.session.rollback()

    def teardown(self, _) -> None:
        self.session.close()


db = Database()
