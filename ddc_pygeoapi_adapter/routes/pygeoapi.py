import logging

from flask import Blueprint, request, g
import pygeoapi.flask_app
from pygeoapi.flask_app import (
    API,
    CONFIG,
    landing_page,
    openapi,
    conformance,
    collections,
    collection_queryables,
    collection_items,
    collection_coverage,
    collection_coverage_domainset,
    collection_coverage_rangetype,
    get_collection_edr_query,
)

from ..db import db
from ..models import Order

# NOTE: this is a workaround for metadata keywords being set from a comma-separated
#       string in the dotenv file, which is not supported by pygeoapi.
if isinstance(CONFIG["metadata"]["identification"]["keywords"], str):
    CONFIG["metadata"]["identification"]["keywords"] = CONFIG["metadata"][
        "identification"
    ]["keywords"].split(",")

# Recreate the pygeoapi API object with our modified config and inject it into
# pygeoapi's `flask_app` module so that it can be used by the pygeoapi blueprint.
pygeoapi.flask_app.api_ = API(CONFIG)

# NOTE: This is our own version of the pygeoapi blueprint that's been modified
#       to support applying our authentication and logging middleware.
#
#       The original blueprint does not support this, because it registers itself
#       with pygeoapi's Flask app, which yields errors (e.g. "The setup method {}
#       can no longer be called on the blueprint {}. It has already been registered
#       at least once."). This is a workaround for that.
pygeoapi_bp = Blueprint("pygeoapi", __name__)

pygeoapi_bp.add_url_rule("/", "landing_page", landing_page)
pygeoapi_bp.add_url_rule("/openapi", "openapi", openapi)
pygeoapi_bp.add_url_rule("/conformance", "conformance", conformance)
pygeoapi_bp.add_url_rule("/collections", "collections", collections)
pygeoapi_bp.add_url_rule("/collections/<path:collection_id>", "collection", collections)
pygeoapi_bp.add_url_rule(
    "/collections/<path:collection_id>/queryables",
    "collection_queryables",
    collection_queryables,
)
pygeoapi_bp.add_url_rule(
    "/collections/<path:collection_id>/items", "collection_items", collection_items
)
pygeoapi_bp.add_url_rule(
    "/collections/<path:collection_id>/items/<item_id>",
    "collection_item",
    collection_items,
)

# NOTE: see `/collections/<path:collection_id>/coverage` below for the coverage routes
pygeoapi_bp.add_url_rule(
    "/collections/<path:collection_id>/coverage/domainset",
    "collection_coverage_domainset",
    collection_coverage_domainset,
)
pygeoapi_bp.add_url_rule(
    "/collections/<path:collection_id>/coverage/rangetype",
    "collection_coverage_rangetype",
    collection_coverage_rangetype,
)

# NOTE: this is the subset of the EDR API routes that's arguably applicable to soil data
#       (none of this is implemented yet, but it's here to remind us of what we need to do)
pygeoapi_bp.add_url_rule(
    "/collections/<path:collection_id>/position",
    "collection_edr_query",
    get_collection_edr_query,
)
pygeoapi_bp.add_url_rule(
    "/collections/<path:collection_id>/area",
    "collection_edr_query",
    get_collection_edr_query,
)
pygeoapi_bp.add_url_rule(
    "/collections/<path:collection_id>/cube",
    "collection_edr_query",
    get_collection_edr_query,
)


@pygeoapi_bp.route("/collections/<path:collection_id>/coverage")
def collection_coverage_wrapped(collection_id):
    g.order = Order(
        collection_id=collection_id,
        client_id=g.token.client_id,
        bbox=request.args.get("bbox"),
    )

    try:
        if "bbox" not in request.args:
            g.order.settle(fulfilled=False, error="No bbox provided")
            return {
                "code": "InvalidParameterValue",
                "description": "No bbox provided",
            }, 400

        result = collection_coverage(collection_id)

        if result.status_code == 200:
            g.order.settle(fulfilled=True)
        else:
            g.order.settle(fulfilled=False, error=result.get_data(True))
    finally:
        db.session.add(g.order)
        db.session.commit()

    return result


# Handle exceptions raised by pygeoapi
@pygeoapi_bp.errorhandler(500)
def handle_exception(error):
    order = getattr(g, "order", None)

    if order is not None:
        order.settle(fulfilled=False, error=str(error))
        db.session.commit()


# Handle errors logged by pygeoapi
class PygeoapiLoggingHandler(logging.StreamHandler):
    def emit(self, record):
        if record.levelno == logging.ERROR:
            order = getattr(g, "order", None)

            if order is not None:
                order.settle(fulfilled=False, error=record.getMessage())
                db.session.commit()


logging.getLogger().addHandler(PygeoapiLoggingHandler())
