from datetime import datetime
from flask import Blueprint, g, request

from ..db import db
from ..models import Order, Token

orders_bp = Blueprint("orders", __name__)


@orders_bp.route("/", methods=["GET"])
def list_orders():
    """List orders

    Returns a list of orders for the current client, ordered by date descending.
    Possible query parameters are `start` and `end` to filter by date.
    Both parameters are expected to be in ISO 8601 format (e.g. `YYYY-MM-DD`).

    If the `start` parameter is not provided, the start date is set to the
    beginning of the current month.
    If the `end` parameter is not provided, the end date is set to the current
    date.
    ---

    # OpenAPI 3.0.2
    description: List orders
    parameters:
      - name: start
        in: query
        description: Start date
        required: false
        schema:
          type: string
          format: date
      - name: end
        in: query
        description: End date
        required: false
        schema:
          type: string
          format: date

    responses:
      200:
        description: List of orders
        content:
          application/json:
            schema:
              type: object
              properties:
                orders:
                  type: array
                  items:
                    type: object
                    properties:
                      id:
                        type: integer
                      collection_id:
                        type: string
                      bbox:
                        type: array
                        items:
                          type: number
                      area:
                        type: number
                      ts:
                        type: string
                      status:
                        type: string
                        enum:
                          - fulfilled
                          - failed
    """

    start = request.args.get("start")
    end = request.args.get("end")

    if start is None:
        start = datetime.now().replace(day=1).isoformat()[:10]
    if end is None:
        end = datetime.now().isoformat()[:10]

    try:
        start = datetime.fromisoformat(start)
        end = datetime.fromisoformat(end)
    except ValueError:
        return {"code": "InvalidDate", "description": "Invalid date format"}, 400

    token: Token = g.token
    orders = (
        db.session.query(Order)
        .filter(Order.client_id == token.client_id)
        .filter(Order.ts >= start.timestamp())
        .filter(Order.ts <= end.timestamp())
        .order_by(Order.ts.desc())
        .all()
    )

    return {
        "orders": [
            {
                "id": order.id,
                "collection_id": order.collection_id,
                "bbox": [float(x) for x in order.bbox.split(",")],
                "area": order.area,
                "ts": datetime.fromtimestamp(order.ts).isoformat(),
                "status": "fulfilled" if order.fulfilled else "failed",
            }
            for order in orders
        ]
    }
