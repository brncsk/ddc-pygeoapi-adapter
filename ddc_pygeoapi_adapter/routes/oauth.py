from flask import Blueprint, session
from ..oauth2 import authorization

bp = Blueprint("oauth", __name__)


@bp.route("/token", methods=["GET", "POST"])
def issue_token():
    """Authorization endpoint for issuing tokens

      Uses OAuth 2's Client Credentials flow to issue tokens
      ---

    parameters:
      - name: grant_type
        in: formData
        type: string
        required: true
        enum:
          - client_credentials
      - name: client_id
        in: formData
        type: string
        required: true
      - name: client_secret
        in: formData
        type: string
        required: true
    responses:
      200:
        description: Token issued
        schema:
          type: object
          properties:
            access_token:
              type: string
            token_type:
              type: string
            expires_in:
              type: integer
      400:
        description: Bad request
    """
    response = authorization.create_token_response()

    # Store the token in the session
    if response.status_code == 200:
        session["token"] = response.json["access_token"]

    return response
