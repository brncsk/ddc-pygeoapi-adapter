"""
    Functions for fetching map list from DoSoReMi and generating pygeoapi resources
    from it.

    Most of the complexity in this module is due to the fact that ArcGIS Server's
    WMS and WCS services use different IDs for the same layer (layers are counted
    backwards in WCS, so layer 0 in WMS is the last layer in WCS). This is why we need
    to fetch the WMS layer's title from the WMS capabilities document and use it
    to find the corresponding WCS coverage ID in the WCS 1.0.0 capabilities document
    (the WCS 2.0.0 capabilities document does not contain the coverage IDs).

    This lets DoSoReMi admins only specify the WMS layer ID in the WordPress admin
    panel and the adapter will automatically find the corresponding WCS coverage ID
    and use it in the pygeoapi config.

    So the workflow is as follows:

    1. Fetch the map list from DoSoReMi
    2. For each map, fetch the WMS capabilities document and find the layer's title
       by its ID
    3. Fetch the WCS capabilities document and find the layer's coverage ID by its
       title
    4. Generate a pygeoapi resource from the map object and the coverage ID
"""

from os import environ
import time
from threading import Thread
import xml.etree.ElementTree as ET

import requests

from pygeoapi.log import LOGGER
from pygeoapi.flask_app import api_, CONFIG

from ddc_pygeoapi_adapter.openapi import generate_openapi


DOSOREMI_MAP_LIST_ENDPOINT = environ.get("DOSOREMI_MAP_LIST_ENDPOINT")
DOSOREMI_MAP_LIST_CACHE_REFRESH_INTERVAL = int(
    environ.get("DOSOREMI_MAP_LIST_CACHE_REFRESH_INTERVAL")
)

WMS_NS = "http://www.opengis.net/wms"
WCS_NS = "http://www.opengis.net/wcs"


# Cache for OGC capabilities documents
ogc_capabilities_cache = {}


def fetch_ogc_capabilities(ogc_url: str):
    """Fetches a capabilities document from an OGC service and caches the result"""

    if ogc_url in ogc_capabilities_cache and (
        time.time() - ogc_capabilities_cache[ogc_url]["timestamp"]
        < DOSOREMI_MAP_LIST_CACHE_REFRESH_INTERVAL
    ):
        return ogc_capabilities_cache[ogc_url]["doc"]

    LOGGER.info("Fetching OGC capabilities from %s", ogc_url)

    service = "WMS" if "WMS" in ogc_url else "WCS"
    version = "2.0.0" if service == "WMS" else "1.0.0"

    capabilities_response = requests.get(
        f"{ogc_url}?service={service}&version={version}&request=GetCapabilities",
        timeout=10,
    )

    capabilities_response.encoding = "utf-8"
    capabilities_text = capabilities_response.text

    capabilities_document = ET.fromstring(
        capabilities_text, ET.XMLParser(encoding="utf-8")
    )

    ogc_capabilities_cache[ogc_url] = {
        "doc": capabilities_document,
        "timestamp": time.time(),
    }

    return capabilities_document


def get_wcs_id_from_wms_id(wms_url: str, wms_layer_id: str):
    """
    Fetches a WMS layer's title from a WMS server by its ID
    and returns the corresponding WCS coverage ID

    :param wms_url: The WMS server's URL
    :param wms_layer_id: The WMS layer's ID
    :return: The WCS coverage ID

    :raises IndexError: If the layer is not found in the WMS capabilities document
    """

    # Fetch the WMS capabilities document and find the layer's name by its ID
    wms_capabilities = fetch_ogc_capabilities(wms_url)
    wms_title = None

    for layer in wms_capabilities.findall(f".//{{{WMS_NS}}}Layer/{{{WMS_NS}}}Layer"):
        label = layer.find(f"{{{WMS_NS}}}Name").text

        if label == wms_layer_id:
            wms_title = layer.find(f"{{{WMS_NS}}}Title").text
            break

    if not wms_title:
        raise IndexError(
            f"Layer {wms_layer_id} not found in WMS capabilities document {wms_url}"
        )

    wcs_capabilities = fetch_ogc_capabilities(wms_url.replace("WMS", "WCS"))

    # Find the layer's coverage ID by its name
    for coverage in wcs_capabilities.findall(f".//{{{WCS_NS}}}CoverageOfferingBrief"):
        label = coverage.find(f"{{{WCS_NS}}}label").text

        if label == wms_title:
            return coverage.find(f"{{{WCS_NS}}}name").text

    raise IndexError(
        f"Layer {wms_title} not found in WCS capabilities document {wms_url}"
    )


def generate_pygeoapi_resources_from_dosoremi():
    """Generate a list of pygeoapi resources from a DOSOREMI map list endpoint"""

    map_list = requests.get(DOSOREMI_MAP_LIST_ENDPOINT, timeout=10).json()
    resources = {
        map_object["slug"]: dosoremi_map_to_pygeoapi_resource(map_object)
        for map_object in map_list
        if map_object["map_meta"]["locale"] == "hu"
        and "wms_url" in map_object["map_meta"]
        and map_object["map_meta"]["wms_url"]
        and "publish_to_ddc" in map_object["map_meta"]
        and map_object["map_meta"]["publish_to_ddc"]
    }

    # Monkey patch the pygeoapi config
    CONFIG["resources"] = resources
    api_.config["resources"] = resources

    # Regenerate the OpenAPI document
    generate_openapi()


def dosoremi_map_to_pygeoapi_resource(map_object):
    """Converts a DoSoReMi map to a pygeoapi resource"""

    title = map_object["title"]
    wms_url = map_object["map_meta"]["wms_url"]
    wms_layer_id = map_object["map_meta"]["main_layer_id"]
    wcs_url = wms_url.replace("WMS", "WCS")
    coverage_id = get_wcs_id_from_wms_id(wms_url, wms_layer_id)

    return {
        "type": "collection",
        "visibility": "default",
        "title": title,
        "description": title,
        "keywords": [],
        "links": [],
        "extents": {
            "spatial": {
                "bbox": list(map(float, environ.get("APP_DEFAULT_BBOX").split(","))),
                "crs": "http://www.opengis.net/def/crs/OGC/1.3/CRS84",
            },
        },
        "providers": [
            {
                "type": "coverage",
                "default": True,
                "name": "rasterio",
                "data": f"WCS:{wcs_url}?version=1.0.0&coverage={coverage_id}",
                "format": {"name": "GTiff", "mimetype": "image/tiff"},
                "options": {"crs": "EPSG:3857"},
            }
        ],
    }


class MapListCacheThread(Thread):
    """Thread that periodically refreshes the map list cache"""

    def __init__(self, interval):
        Thread.__init__(self)
        self.interval = interval

    def run(self):
        while True:
            try:
                generate_pygeoapi_resources_from_dosoremi()
            except Exception as e:
                LOGGER.error("Error while refreshing map list cache: %s", e)

            time.sleep(self.interval)


map_list_cache_thread = MapListCacheThread(DOSOREMI_MAP_LIST_CACHE_REFRESH_INTERVAL)
