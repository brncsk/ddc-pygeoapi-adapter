from os import environ
from pathlib import Path

from flask import Flask

from .db import db
from .dosoremi import generate_pygeoapi_resources_from_dosoremi, map_list_cache_thread
from .oauth2 import config_oauth

from .routes.oauth import bp as oauth_bp
from .routes.pygeoapi import pygeoapi_bp
from .routes.orders import orders_bp

app = Flask(
    __name__,
    static_url_path="/static",
    static_folder=Path(__file__).parent.parent / "assets",
)
app.url_map.strict_slashes = False
app.secret_key = environ.get("SECRET_KEY")
app.config["SWAGGER"] = {"openapi": "3.0.2"}

app.register_blueprint(pygeoapi_bp, url_prefix="/")
app.register_blueprint(oauth_bp, url_prefix="/oauth")
app.register_blueprint(orders_bp, url_prefix="/orders")

db.init_app(app)
config_oauth(app)


@app.before_first_request
def init_app():
    # NOTE: `generate_pygeoapi_resources_from_dosoremi()` takes care of
    #       OpenAPI document generation, so we don't need to call
    #       `generate_openapi()` here.
    generate_pygeoapi_resources_from_dosoremi()
    map_list_cache_thread.start()
