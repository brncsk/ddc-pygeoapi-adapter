#!/bin/bash

# Generate docs from `docs/docs.md` to `docs/docs.pdf`
# using `pandoc` and `mermaid-filter`

cd $(dirname $0)/docs
npm install --global mermaid-filter
pandoc -t pdf -F mermaid-filter -o docs.pdf docs.md
