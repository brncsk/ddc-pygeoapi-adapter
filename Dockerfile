ARG PYGEOAPI_VERSION

FROM geopython/pygeoapi:${PYGEOAPI_VERSION}

RUN pip3 install poetry

ADD pyproject.toml /opt/ddc-pygeoapi-adapter/pyproject.toml
ADD poetry.lock /opt/ddc-pygeoapi-adapter/poetry.lock

WORKDIR /opt/ddc-pygeoapi-adapter

RUN poetry install

ADD . /opt/ddc-pygeoapi-adapter
ENV PYGEOAPI_CONFIG /opt/ddc-pygeoapi-adapter/assets/pygeoapi.config.yml


ENTRYPOINT ["/opt/ddc-pygeoapi-adapter/scripts/docker-entrypoint.sh"]