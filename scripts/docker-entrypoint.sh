#!/bin/bash

SCRIPT_NAME=${SCRIPT_NAME:=/}

WSGI_NAME=${WSGI_NAME:=pygeoapi}
WSGI_HOST=${WSGI_HOST:=0.0.0.0}
WSGI_PORT=${WSGI_PORT:=80}
WSGI_APP=${WSGI_APP:=ddc_pygeoapi_adapter.wsgi:app}

WSGI_WORKERS=${WSGI_WORKERS:=4}
WSGI_WORKER_TIMEOUT=${WSGI_WORKER_TIMEOUT:=6000}
WSGI_WORKER_CLASS=${WSGI_WORKER_CLASS:=gevent}

echo "Start gunicorn name=${CONTAINER_NAME} on ${CONTAINER_HOST}:${CONTAINER_PORT} with ${WSGI_WORKERS} workers and SCRIPT_NAME=${SCRIPT_NAME}"

exec poetry run gunicorn \
    --workers ${WSGI_WORKERS} \
    --worker-class=${WSGI_WORKER_CLASS} \
    --timeout ${WSGI_WORKER_TIMEOUT} \
    --name=${WSGI_NAME} \
    --bind ${WSGI_HOST}:${WSGI_PORT} \
    ${WSGI_APP}
