#!/bin/bash

# This script is used to build and push the Docker image to ECR,
# and then deploy the new task definition to ECS using terraform.


set -e


# Initialize terraform, then selectively apply the ECR resources

echo "Initializing terraform..."

terraform -chdir=terraform init

echo "Creating ECR repository..."

terraform -chdir=terraform apply \
  -auto-approve


# Build and push Docker image to ECR

export ECR_REPO_URL=$(terraform -chdir=terraform output ecr-repo-url | tr -d '"' | sed 's/https:\/\///')
export ECR_USER=$(terraform -chdir=terraform output ecr-user | tr -d '"')

echo "Logging into ECR at $ECR_REPO_URL..."

terraform -chdir=terraform output ecr-password | \
  tr -d '"' | \
  docker login -u ${ECR_USER} --password-stdin $ECR_REPO_URL

echo "Building and pushing Docker image..."

docker compose build pygeoapi
docker compose push pygeoapi

echo "Deploying new task definition to ECS..."

terraform -chdir=terraform apply -auto-approve