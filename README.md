# Adatgazda-oldali PyGeoAPI adapter a Danube Data Cube-hoz

## Futtatás

### Helyi gépen

```bash
PYGEOAPI_CONFIG=./assets/pygeoapi.config.yml poetry run start
```

### Dockerben

```bash
docker compose up
```

### ECS-en

#### Függőségek

- `terraform`
- `aws-cli`
- AWS credential-ök a környezeti változókban

```bash
./scripts/deploy-to-ecs.sh
```

## Production környezet elérése

### Autentikáció

Az adapter használatához OAuth Bearer token szükséges.

A token-végpont a https://ddc-soil.rissac.hu/oauth/token címen érhető el.
A végponton kizárólag a [Client Credentials Flow](https://auth0.com/docs/get-started/authentication-and-authorization-flow/client-credentials-flow) támogatott, `POST`-on keresztül.

A tesztelés ideje alatt a következő credential-ök érvényesek:

- `client_id`: `ddc`
- `client_secret`: `ddc`

### cURL példa

```bash
curl \
  -X POST \
  -H 'Content-Type: application/x-www-form-urlencoded' \
  -d 'client_id=ddc&client_secret=ddc&grant_type=client_credentials' \
  https://ddc-soil.rissac.hu/oauth/token
```

### A szerveren található Coverage-ek listázása

https://ddc-soil.rissac.hu/collections?f=json

### Egy adott Coverage adatainak lekérdezése földrajzi befoglalóval

https://ddc-soil.rissac.hu/collections/agyagtartalom-0-30-cm/coverage?f=GTiff&bbox=19.92292183453057,47.87788842029229,19.94989201453328,47.89409794575795
