# API-design megfontolások az ATK Talajtani Intézet adatainak közzétételéhez

## Metaadatok

Metaadatok két forrásból származhatnak:

1. A `DescribeCoverage` hívás visszaadja a WCS-szerver rétegeinek metaadatait.
   WCS `2.0.1` esetén nem tartalmazza az ArcGIS-rétegnevet, csak a rétegszámot/-azonosítót.
2. A `dosoremi.hu` adatbázisából (valószínűleg jobb).

## Linkek

- https://maps.isric.org/
