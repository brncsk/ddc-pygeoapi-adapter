# Documentation

This folder contains the documentation for the project.

- [`docs/arcgis-server-wcs-test.md`](docs/arcgis-server-wcs-test.md) – Test results for ArcGIS Server 10.5's WCS implementation (in Hungarian)
- [`docs/taki-api-design.md`](docs/taki-api-design.md) – API design considerations for publishing ATK TAKI's data (in Hungarian)
