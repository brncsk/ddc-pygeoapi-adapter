# ArcGIS Server 10.5 WCS teszt

- Működő WCS Verziók: `1.0.0`, `1.1.0`, `1.1.1`, `1.1.2`, `2.0.1` (a WCS 2 `GetCapabilities`
  válasz alapján)

## Tesztadatok befoglalója (Mátra)

```
EPSG:3857:
  BOX(2217809.513734135 6086563.808569879,2220811.8204386393 6089254.555144596)
  2217809.513734135,6086563.808569879,2220811.8204386393,6089254.555144596
  subset=x(2217809.513734135,2220811.8204386393)&subset=y(6086563.808569879,6089254.555144596)

EPSG:4326:
  BOX(19.92292183453057 47.87788842029229,19.94989201453328 47.89409794575795)
  bbox=19.92292183453057,47.87788842029229,19.94989201453328,47.89409794575795
  subset=x(19.92292183453057,19.94989201453328)&subset=y(47.87788842029229,47.89409794575795)
```

## Működő teszthívások

### WCS 1.0.0

| Metódus            | Példa URL                                                                                                                                                                                                                                              |
| ------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `GetCapabilities`  | https://maps.rissac.hu:6443/arcgis/services/DDC/MapServer/WCSServer?service=WCS&version=1.0.0&request=GetCapabilities                                                                                                                                  |
| `DescribeCoverage` | https://maps.rissac.hu:6443/arcgis/services/DDC/MapServer/WCSServer?service=WCS&version=1.0.0&request=DescribeCoverage&coverage=1                                                                                                                      |
| `GetCoverage`      | https://maps.rissac.hu:6443/arcgis/services/DDC/MapServer/WCSServer?service=WCS&version=1.0.0&request=GetCoverage&coverage=1&bbox=19.92292183453057,47.877888420292294,19.949892014533276,47.89409794575795&RESX=1&RESY=1&CRS=EPSG:4326&FORMAT=GeoTIFF |

#### Megjegyzések

- A DescribeCoverage hívás visszaadja a rétegnevet (lehet, hogy nem szükséges
  hosszútávon)
- A GetCoverage hívásban a rétegnév helyett a rétegszámot kell megadni
  (`Coverage1` (WCS 2) helyett `1` (WCS 1))
- A `RESX` és `RESY` paramétereket nem kell megadni, ha a `WIDTH` és `HEIGHT`
  paraméterekkel megadjuk a kép felbontását, de nem tudni, hogy ezeket milyen
  mértékegységben kell megadni (px/m-ben, vagy m/px-ben).

### WCS 2.0.1

| Metódus            | Példa URL                                                                                                                                                                                                                                             |
| ------------------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `GetCapabilities`  | https://maps.rissac.hu:6443/arcgis/services/DDC/MapServer/WCSServer?service=WCS&version=2.0.1&request=GetCapabilities                                                                                                                                 |
| `DescribeCoverage` | https://maps.rissac.hu:6443/arcgis/services/DDC/MapServer/WCSServer?service=WCS&version=2.0.1&request=DescribeCoverage&coverageid=Coverage1                                                                                                           |
| `GetCoverage`      | https://maps.rissac.hu:6443/arcgis/services/DDC/MapServer/WCSServer?service=WCS&version=2.0.1&request=GetCoverage&coverageid=Coverage1&format=image/tiff&subset=x(2217809.513734135,2220811.8204386393)&subset=y(6086563.808569879,6089254.555144596) |

#### Megjegyzések

- A `format` tartalma MIME
- A `subset` paraméterek CRS-e EPSG:3857
- Alapból `multipart/mixed` formátumban adja vissza a képet,
  akkor is, ha nincs megadva a `mediaType=multipart/mixed` paraméter.
  Ez elvileg ellentmond a WCS-szabványnak (ld.
  https://github.com/MapServer/MapServer/issues/6551).
- A GDAL támogatja a multipart-válaszokat:
  https://github.com/OSGeo/gdal/blob/26a510cadacc9f0284a65b8e16c9004396dfe11f/frmts/wcs/wcsdataset.cpp#L688-L713
- WCS 2.0 esetén nem kell megadni a `RESX` és `RESY` paramétereket,
  a fenti hívás a natív felbontású képet adja vissza.

## Nyitott kérdések

- `SUBSETTINGCRS` paraméter működik-e?
