# 1. Felhasználói dokumentáció

A rendszer célja a DoSoReMi-integráció keretében a Duna Adatkocka rendszerébe történő publikációja, a Duna Adatkocka felől történő adatlekérdezés, valamint a Duna Adatkocka felé történő adatexport biztosítása, illeve az elérések naplózása.

## 1.1. A rendszer elérhetősége

A rendszer a tesztidőszak alatt böngészőből a [https://ddc-soil.rissac.hu/](https://ddc-soil.rissac.hu/) címen érhető el.

Az azonosításhoz a [1.1.2.](#112-azonosítás) pontban leírtak szerint kell eljárni.

## 1.1.2. Azonosítás

A felület eléréséhez azonosítás szükséges, az alábbi adatokkal:

- Elérési út: https://ddc-soil.rissac.hu/oauth/token
- Felhasználói név: `ddc`
- Jelszó: `ddc`

Az azonosítást követően elérhetővé válik a rendszer főoldala.

A tesztidőszak végeztével a webes felület böngészőből történő elérése megszűnik, a rendszer csak a DoSoReMi-integráció révén, illetve a Duna Adatkocka felől, API-n keresztül elérhető.

## 1.2. DoSoReMi-integráció

Az egyes talajadat-térképek publikációja a DoSoReMi adminisztrációs felületén keresztül
([https://dosoremi.hu/wp-admin/](https://dosoremi.hu/wp-admin/)) történik. A publikációhoz
az egyes térképeknél be kell kapcsolni a „Publikálás a Duna Adatkockába” opciót, majd
a „Mentés” gombra kattintani.

![DoSoReMi adminisztrációs felület](dosoremi-admin.png)

Néhány percen belül a térkép megjelenik a Duna Adatkocka felületén.

# 2. Fejlesztői dokumentáció

## 2.1. Architektúra

```mermaid
flowchart TD
  subgraph ATK TAKI
    ddc-pygeoapi-adapter <-.-> arcgis[(ArcGIS szerver\nmaps.rissac.hu)]
    subgraph ddc-soil.rissac.hu
      subgraph Docker
        Traefik
        Portainer
        subgraph ddc-pygeoapi-adapter
          PyGeoAPI <--> sqlite[("SQLite Audit Log")]
        end
      end
    end
  end
  Duna_Adatkocka <--> ddc-soil.rissac.hu
  DoSoReMi <--> ddc-soil.rissac.hu

  classDef db fill:white,stroke:#333,stroke-width:1px;
  classDef server fill:#ddd,stroke:black,stroke-width:1px;

  class Duna_Adatkocka server;
  class DoSoReMi server;

  class sqlite db;
  class arcgis db;
```

## 2.2. Üzemeltetés

A rendszer a Talajtani Kutatóintézet `ddc-soil.rissac.hu` szerverén fut docker környezetben.
A szerver a TAKI belső hálózatán érhető el, kívülről az eléréshez VPN-kapcsolat szükséges.

## 2.2.1. Docker-konténerek

A környezet az alábbi konténereket foglalja magában:

- Traefik: reverse proxy, a különböző konténerekhez tartozó domainek alapján irányítja a forgalmat
- Portainer: konténerek adminisztrációs felülete
- ddc-pygeoapi-adapter: a DoSoReMi-integrációért felelős Python alkalmazás

A `ddc-pygeoapi-adapter` konténer a [PyGeoAPI](https://pygeoapi.io/) alkalmazásra épül, amely egy OGC API Features szabványt megvalósító Python alkalmazás. Az alkalmazás a DoSoReMi API-ján keresztül lekérdezi a publikált térképeket, és publikálja azokat a Duna Adatkocka felé.

## 2.2.2. SQLite adatbázis

A `ddc-pygeoapi-adapter` konténer az OAuth-tokenek és -kliensek tárolását, valamint
az audit log célú naplózást egy SQLite adatbázisban végzi.

Az adatbázis a `/opt/ddc-pygeoapi-adapter/ddc-pygeoapi.db` fájlban található.

Az adatbázis az alábbi táblákat tartalmazza:

### `order`

Az `order` tábla a Duna Adatkocka felől érkező megrendelések (adatlekérdezések)
adatait tartalmazza.

| Oszlop          | Típus     | Leírás                                                |
| --------------- | --------- | ----------------------------------------------------- |
| `id`            | `INTEGER` | A rekord egyedi azonosítója                           |
| `client_id`     | `TEXT`    | A megrendelő azonosítója (mindig `ddc`)               |
| `collection_id` | `TEXT`    | A lekérdezett térkép azonosítója                      |
| `bbox`          | `TEXT`    | A lekérdezés földrajzi kiterjedése                    |
| `ts`            | `TEXT`    | A lekérdezés időpontja                                |
| `fulfilled`     | `BOOL`    | A lekérdezés teljesítésének állapota (`true`/`false`) |
| `time_taken_ms` | `INTEGER` | A lekérdezés teljesítéséhez szükséges idő (ms)        |
| `error`         | `TEXT`    | Hibaüzenet, ha a lekérdezés teljesítése nem sikerült  |

### `oauth_client`

Az OAuth 2 kliensek adatait tartalmazza.

| Oszlop          | Típus  | Leírás                 |
| --------------- | ------ | ---------------------- |
| `client_id`     | `TEXT` | A kliens azonosítója   |
| `client_secret` | `TEXT` | A kliens titkos kulcsa |

### `oauth_token`

Az OAuth 2 access tokenek adatait tartalmazza.

| Oszlop         | Típus     | Leírás                                                                                 |
| -------------- | --------- | -------------------------------------------------------------------------------------- |
| `access_token` | `TEXT`    | Az access token értéke                                                                 |
| `client_id`    | `TEXT`    | A kliens azonosítója                                                                   |
| `user_id`      | `TEXT`    | A felhasználó azonosítója (mindig `null` mivel `client_credentials` flow-t használunk) |
| `expired_at`   | `TEXT`    | Az access token lejárati ideje                                                         |
| `expires_in`   | `INTEGER` | Az access token lejárati ideje másodpercben                                            |
| `token_type`   | `TEXT`    | Az access token típusa (mindig `Bearer`)                                               |

## 2.2.3. User flow-k és szekvenciadiagramok

## 2.2.3.1. Azonosítás

Az azonosítás OAuth2 segítségével történik.
Kizárólag az OAuth 2 Client Credentials flow támogatott `POST`-ként (`application/x-www-form-urlencoded`), a tesztelés ideje alatt HTTP Basic Auth-tal kiegészítve a böngészőbeli teszteléshez.

A teszt alatt mindkét módszer a `ddc:ddc` (`client_id:client_secret`) credentialökkel használható. A Basic Auth flow az access tokent sütiként tárolja, ez a tesztidőszak végeztével ki lesz kapcsolva.

Az azonosítás menetét az alábbi diagra mutatja be:

```mermaid
  sequenceDiagram
    participant AUDIT as Audit log
    participant ADAPTER as DDC <-> DoSoReMi Adapter
    participant DDC as Duna Adatkocka

  Note right of DDC: OAuth2 autentikáció
  DDC ->> ADAPTER: POST /oauth/token
  ADAPTER ->> DDC: 200 OK (access token)
```

## 2.2.3.2. Térképek lekérdezése a DoSoReMi-től

A rendszer a DoSoReMi-ben közzétett térképeket a megadott időközönként lekérdezi, és publikálja a Duna Adatkocka felé.

```mermaid
  sequenceDiagram
    participant DOSOREMI as DoSoReMi
    participant AUDIT as Audit log (SQLite)
    participant ADAPTER as DDC <-> DoSoReMi Adapter
    participant DDC as Duna Adatkocka

    loop Minden 5 percben
      Note right of DDC: Térképek lekérdezése a DoSoReMi-től
      ADAPTER ->> DOSOREMI: GET https://www.dosoremi.hu/wp-json/wp/v2/maps
      DOSOREMI ->> ADAPTER: 200 OK (Térképek listája JSON formátumban)
      ADAPTER ->> DDC: POST /soilmaps
      DDC ->> ADAPTER: 200 OK
    end
```

#### 2.2.3.3. Térképi adatok lekérdezése a DoSoReMi-től

1.  A Duna Adatkocka első lépésben lekérdezi a DoSoReMi-ben közzétett térképek listáját a `/collections` végponton keresztül.
    Ezt az adapter az [OGC API - Features](https://docs.ogc.org/is/17-069r3/17-069r3.html#_collections_) szabvány szerinti formátumban szolgáltatja.

2.  Az elérhető térképek ismeretében lehetőség van az egyes térképekhez tartozó
    földrajzi kiterjedés lekérdezésére a `/collections/<collection>/coverage/domainSet` végponton keresztül.

    A válaszban a kiterjedés `minx, maxx, miny, maxy` paramétereinek vonatkozási rendszere `HD72/EOV (EPSG:23700)`.

3.  A tényleges téradatok lekérdezéséhez a `/collections/<collection>/coverage` végpont használható.

    A bbox paraméter vonatkozási rendszere WGS'84 (EPSG:4326), értéke vesszővel elválasztva: `lon_min,lat_min,lon_max,lat_max`. Az `f` paraméter értéke csak GTiff lehet.

    Pl.: https://ddc-soil.rissac.hu/collections/agyagtartalom-0-30-cm/coverage?f=GTiff&bbox=19.922,47.877,19.949,47.894

```mermaid
  sequenceDiagram
    participant ARCGIS as ArcGIS Server
    participant AUDIT as Audit log (SQLite)
    participant ADAPTER as DDC <-> DoSoReMi Adapter
    participant DDC as Duna Adatkocka

    Note right of DDC: Elérhető térképek lekérdezése
    DDC -->> ADAPTER: GET /collections
    ADAPTER ->> DDC: 200 OK (Térképek listája JSON formátumban)

    Note right of DDC: Térkép földrajzi kiterjedésének lekérdezése
    DDC -->> ADAPTER: GET /collections/<collection>/coverage/domainSet
    ADAPTER ->> DDC: 200 OK (Térkép földrajzi kiterjedése)

    #  https://ddc-soil.rissac.hu/collections/agyagtartalom-0-30-cm/coverage?f=GTiff&bbox=19.922,47.877,19.949,47.894
    Note right of DDC: Térképi adatok lekérdezése
    DDC -->> ADAPTER: GET /collections/<collection>/coverage?f=GTiff&bbox=<bbox>
    # https://maps.rissac.hu:6443/arcgis/services/dosoremi_web_mercator/MapServer/WCSServer
    ADAPTER ->> ARCGIS: GET /arcgis/rest/services/<collection>/MapServer/WCSServer
    ARCGIS ->> ADAPTER: 200 OK (Térképi adatok GeoTIFF formátumban)
    ADAPTER -->> AUDIT: INSERT INTO audit_log (...) VALUES (...)
    ADAPTER ->> DDC: 200 OK (Térképi adatok GeoTIFF formátumban)
```

#### 2.2.3.4. A rendelések lekérdezése

A DoSoReMi-ben közzétett rendelések lekérdezése a `/orders` végponton keresztül történik.

Opcionálisan megadhatóak a `start` és `end` paraméterek (`YYYY-MM-DD`) az intervallum beállításához.
Alapértelmezésben a folyó hónapban beérkezett rendelésekkel tér vissza.

A válasz tartalmazza a rendelések időpontját, a kért terület befoglalóját és méretét (négyzetméterben). Amennyiben a rendelés teljesült (a `/coverage` végpont HTTP 200-zal tért vissza), a `status` paraméter értéke `fulfilled`, egyébként (hiba esetén) `failed`.

```mermaid
  sequenceDiagram
    participant AUDIT as Audit log (SQLite)
    participant ADAPTER as DDC <-> DoSoReMi Adapter
    participant DDC as Duna Adatkocka

    Note right of DDC: Rendelések lekérdezése a DoSoReMi-től
    DDC -->> ADAPTER: GET /orders
    ADAPTER -->> AUDIT: SELECT * FROM orders
    ADAPTER ->> DDC: 200 OK (Rendelések listája JSON formátumban)
```
