output "app-url" {
  value = module.networking.app-url
}

output "ecr-repo-url" {
  value = module.ecr.ecr-repo-url
}

output "ecr-user" {
  value = module.ecr.ecr-user
}

output "ecr-password" {
  value     = module.ecr.ecr-password
  sensitive = true
}