data "dotenv" "dotenv" {
  filename = "../.env"
}

locals {
  app_name = data.dotenv.dotenv.env.APP_NAME

  aws_region         = data.dotenv.dotenv.env.AWS_REGION
  availability_zones = split(",", data.dotenv.dotenv.env.AWS_AVAILABILITY_ZONES)
  vpc_cidr           = data.dotenv.dotenv.env.AWS_VPC_CIDR
  route53_zone_id    = data.dotenv.dotenv.env.AWS_ROUTE53_ZONE_ID
}
