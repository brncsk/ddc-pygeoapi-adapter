module "networking" {
  source             = "./modules/networking"
  app_name           = local.app_name
  availability_zones = local.availability_zones
  vpc_cidr           = local.vpc_cidr
  route53_zone_id    = local.route53_zone_id
}

module "ecr" {
  source   = "./modules/ecr"
  app_name = local.app_name
}

module "ecs" {
  source                         = "./modules/ecs"
  aws_region                     = local.aws_region
  app_name                       = local.app_name
  app_domain                     = module.networking.app-domain
  service-subnet-ids             = module.networking.ecs-subnet-ids
  service-security-group-ids     = module.networking.ecs-security-group-ids
  load-balancer-target-group-arn = module.networking.ecs-load-balancer-target-group-arn
  ecr-repo-url                   = module.ecr.ecr-repo-url
  ecs-task-role-arn              = module.networking.ecs-task-role-arn
}
