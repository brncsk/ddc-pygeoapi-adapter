resource "aws_ecs_cluster" "pygeoapi" {
  name = "pygeoapi"
}

resource "aws_cloudwatch_log_group" "log-group" {
  name = "${var.app_name}-logs"
}

resource "aws_ecs_task_definition" "pygeoapi" {
  family                   = "pygeoapi"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = "256"
  memory                   = "512"
  execution_role_arn       = var.ecs-task-role-arn
  task_role_arn            = var.ecs-task-role-arn

  container_definitions = jsonencode(
    [
      {
        name        = "pygeoapi",
        image       = "${replace(var.ecr-repo-url, "https://", "")}/${var.app_name}:latest",
        essential   = true,
        networkMode = "awsvpc",
        portMappings = [
          {
            containerPort = 80,
            hostPort      = 80
          }
        ],
        environment = [
          {
            name  = "PYGEOAPI_CONFIG",
            value = "/opt/ddc-pygeoapi-adapter/assets/pygeoapi.config.yml"
          },
          {
            name  = "APP_DOMAIN",
            value = var.app_domain
          }
        ],
        logConfiguration = {
          logDriver = "awslogs",
          options = {
            awslogs-group         = "${aws_cloudwatch_log_group.log-group.name}",
            awslogs-region        = "${var.aws_region}",
            awslogs-stream-prefix = "pygeoapi"
          }
        }
      }
  ])
}

resource "aws_ecs_service" "pygeoapi" {
  name                = "pygeoapi"
  cluster             = aws_ecs_cluster.pygeoapi.id
  task_definition     = aws_ecs_task_definition.pygeoapi.arn
  desired_count       = 1
  launch_type         = "FARGATE"
  scheduling_strategy = "REPLICA"

  network_configuration {
    subnets          = var.service-subnet-ids
    security_groups  = var.service-security-group-ids
    assign_public_ip = false
  }

  load_balancer {
    target_group_arn = var.load-balancer-target-group-arn
    container_name   = "pygeoapi"
    container_port   = 80
  }
}
