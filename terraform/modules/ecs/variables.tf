variable "app_name" {
  description = "Name of the application"
  type        = string
}

variable "app_domain" {
  description = "Domain of the application"
  type        = string
}

variable "aws_region" {
  description = "AWS region to use"
  type        = string
}

variable "service-subnet-ids" {
  description = "Subnet IDs to use for the service"
  type        = list(string)
}

variable "service-security-group-ids" {
  description = "List of security group IDs to attach to the ECS service"
  type        = list(string)
}

variable "load-balancer-target-group-arn" {
  description = "ARN of the load balancer target group"
  type        = string
}


variable "ecr-repo-url" {
  description = "Repository URL for the ECR repository"
  type        = string
}

variable "ecs-task-role-arn" {
  description = "ARN of the ECS task role"
  type        = string
}
