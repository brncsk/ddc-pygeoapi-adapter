variable "app_name" {
  description = "Name of the application"
  type        = string
}

variable "availability_zones" {
  description = "Availability zones to use"
  type        = list(string)
}

variable "vpc_cidr" {
  description = "CIDR block for the VPC"
  type        = string
}

variable "route53_zone_id" {
  description = "Route53 zone ID"
  type        = string
}