resource "aws_alb" "application-load-balancer" {
  name               = "${var.app_name}-alb"
  internal           = false
  load_balancer_type = "application"
  subnets            = aws_subnet.public.*.id
  security_groups    = [aws_security_group.load-balancer-security-group.id]

  enable_deletion_protection = false

  tags = {
    Name = "${var.app_name}-alb"
  }
}

resource "aws_lb_target_group" "target-group" {
  name        = "${var.app_name}-tg"
  port        = 80
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = aws_vpc.aws-vpc.id

  health_check {
    healthy_threshold   = "3"
    interval            = "30"
    protocol            = "HTTP"
    matcher             = "200"
    timeout             = "3"
    path                = "/"
    unhealthy_threshold = "2"
  }

  tags = {
    Name = "${var.app_name}-lb-tg"
  }
}

resource "aws_lb_listener" "https-listener" {
  load_balancer_arn = aws_alb.application-load-balancer.id
  port              = "443"
  protocol          = "HTTPS"
  certificate_arn   = element(aws_acm_certificate.cert.*.arn, count.index)

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.target-group.id
  }

  count = var.route53_zone_id == "" ? 0 : 1
}

resource "aws_lb_listener" "http-listener" {
  load_balancer_arn = aws_alb.application-load-balancer.id
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}
