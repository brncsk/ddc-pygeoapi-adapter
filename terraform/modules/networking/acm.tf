resource "aws_acm_certificate" "cert" {
  domain_name       = element(aws_route53_record.taki-pygeoapi, count.index).name
  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }

  count = var.route53_zone_id == "" ? 0 : 1
}

resource "aws_acm_certificate_validation" "cert" {
  validation_record_fqdns = element(aws_acm_certificate.cert, count.index).domain_validation_options.*.resource_record_name
  certificate_arn         = element(aws_acm_certificate.cert, count.index).arn

  count = var.route53_zone_id == "" ? 0 : 1
}
