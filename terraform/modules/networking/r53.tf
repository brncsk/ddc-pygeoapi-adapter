data "aws_route53_zone" "domain" {
  name         = var.route53_zone_id
  private_zone = false

  count = var.route53_zone_id == "" ? 0 : 1
}

resource "aws_route53_record" "taki-pygeoapi" {
  zone_id = element(data.aws_route53_zone.domain, count.index).id
  name    = "${var.app_name}.${element(data.aws_route53_zone.domain, count.index).name}"

  type    = "CNAME"
  ttl     = 60
  records = [aws_alb.application-load-balancer.dns_name]

  count = var.route53_zone_id == "" ? 0 : 1
}

resource "aws_route53_record" "cert-validations" {
  allow_overwrite = true
  count           = (var.route53_zone_id != "") ? length(aws_acm_certificate.cert[0].domain_validation_options) : 0
  ttl             = 60

  zone_id = data.aws_route53_zone.domain[0].id

  name    = element(aws_acm_certificate.cert[0].domain_validation_options.*.resource_record_name, count.index)
  type    = element(aws_acm_certificate.cert[0].domain_validation_options.*.resource_record_type, count.index)
  records = [element(aws_acm_certificate.cert[0].domain_validation_options.*.resource_record_value, count.index)]
}
