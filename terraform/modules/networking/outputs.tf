output "ecs-task-role-arn" {
  value = aws_iam_role.ecs-task-execution-role.arn
}

output "ecs-subnet-ids" {
  value = aws_subnet.private.*.id
}

output "ecs-security-group-ids" {
  value = [
    aws_security_group.load-balancer-security-group.id,
    aws_security_group.service-security-group.id
  ]
}

output "ecs-load-balancer-target-group-arn" {
  value = aws_lb_target_group.target-group.arn
}

output "app-url" {
  value = (
    var.route53_zone_id == ""
    ? "https://${aws_alb.application-load-balancer.dns_name}"
    : "https://${var.app_name}.${data.aws_route53_zone.domain[0].name}"
  )
}
