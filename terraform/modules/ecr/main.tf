# NOTE: names of the following resources are referenced in the
#       scripts/deploy-to-ecs.sh script

resource "aws_ecr_repository" "ecr-repo" {
  name                 = var.app_name
  image_tag_mutability = "MUTABLE"
  force_delete = true
}

resource "aws_ecr_repository_policy" "ecr-repo-policy" {
  repository = aws_ecr_repository.ecr-repo.name
  policy     = data.aws_iam_policy_document.ecr-repo-policy.json
}

data "aws_iam_policy_document" "ecr-repo-policy" {
  statement {
    actions = [
      "ecr:BatchCheckLayerAvailability",
      "ecr:BatchGetImage",
      "ecr:GetDownloadUrlForLayer",
      "ecr:GetRepositoryPolicy",
      "ecr:GetAuthorizationToken",
      "ecr:DescribeRepositories",
      "ecr:ListImages",
      "ecr:DescribeImages",
      "ecr:PutImage",
      "ecr:InitiateLayerUpload",
      "ecr:UploadLayerPart",
      "ecr:CompleteLayerUpload"
    ]

    effect = "Allow"

    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
  }
}

data "aws_ecr_authorization_token" "ecr-auth-token" {
  registry_id = aws_ecr_repository.ecr-repo.registry_id
}
