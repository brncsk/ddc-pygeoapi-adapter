output "ecr-repo-url" {
  value = data.aws_ecr_authorization_token.ecr-auth-token.proxy_endpoint
}

output "ecr-user" {
  value = data.aws_ecr_authorization_token.ecr-auth-token.user_name
}

output "ecr-password" {
  value     = data.aws_ecr_authorization_token.ecr-auth-token.password
  sensitive = true
}
